@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                     <div class="col-md-16 content-area">
                        @if ($message = Session::get('success'))
                        <div class="row justify-content-md-center">
                            <div class="col-md-5">
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <p>{{ $message }}</p>
                                </div>
                            </div>
                        </div>
                        @endif
                       
                    </div>
                    <div class="col-md-5">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

</div>
                </div>
            {!! Form::open(array('url' => '/home/add', 'method' => 'POST')) !!}
            <div class="form-group col-md-6 ">
            <h3><i class='fa fa-calendar-plus-o'></i> Add Value</h3>
             </div>
     <div class="form-group col-md-6 ">
        {!! Form::label('name', 'Value') !!}
        {!! Form::text('vtitle', '', array('class' => 'form-control')) !!}
    </div>
    
    <div class="col-md-12 text-center">
    <!--{!! Form::submit('Add Event', array('class' => 'btn btn-primary')) !!}-->
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
   
</div>
    {!! Form::close() !!}
 </div>
    </div>
</div>

            </div>
        </div>
    </div>
</div>
@endsection
