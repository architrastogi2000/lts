<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSValues extends Model
{
    protected $fillable = ['Value'];
    protected $table='msvalues' ;

   public function save_data($data) {
        $ms = new MSValues();
        $ms->Value = $data['value'];
       
       // save_data
        $ms->save();
		return $ms->id;
    }	
}
