<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MSValues;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function add(Request $request)
    {
       // return view('home');
         $this->validate($request, [
            'vtitle' => 'required|max:50|alpha_num',            
            
        ]);
         $data['value'] = $request->vtitle;
         $ms = new MSValues();      
         $msD = $ms->save_data($data);      
      
       
                
        return redirect('/home')->with('success','Value has been saved successfully');   
    }
}
